import requests
import hashlib
from hashdb_client.constants import HASH_TYPES

class APIClient(object):
    def __init__(self, base_url='https://hashdb.insomnia247.nl/v1'):
        self.base_url = base_url

    def check_plaintext(self, password):
        hash_type = 'sha256'
        hash = hashlib.sha256( password.encode('utf-8') )
        return self.check(hash_type=hash_type, hash=hash.hexdigest())

    def check(self, hash_type, hash):
        if hash_type not in HASH_TYPES:
            raise ValueError(f'hash_type not in {HASH_TYPES}')
        self.response = requests.get(f'{self.base_url}/{hash_type}/{hash}')
        self.response.raise_for_status()
        return self.response.json()

