import hashlib

HASH_TYPES = [
    'md5',
    'sha1',
    'sha256'
]
