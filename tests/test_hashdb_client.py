from hashdb_client import __version__
from hashdb_client import APIClient

mock_response = {'hash': '787e2c8941aa58a0c1e2cd87b0bdbfc309e4d3d919c0ab520e30a73f99b0a448', 'found': True, 'result': 'ironfloor881'}

def test_version():
    assert __version__ == '0.1.0'


def test_check_plaintext():
    c = APIClient()
    c.check_plaintext('ironfloor881')
